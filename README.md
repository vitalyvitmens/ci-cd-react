#### Для создания подобного приложения:

### Инициализируем npm в какой-нибудь директории:
- npm init -y

### Ставим экспресс node-fetch и зависимости:
- npm i express node-fetch

### Создаем файлик index.js в котором пишем свой сервер:
```
const express = require('express')
const { default: fetch } = require('node-fetch')

const app = express()

const DATE_SERVER_HOST = process.env.DATE_SERVER_HOST || 'http://localhost:3005'

app.get('/', (req, res) => {
  fetch(DATE_SERVER_HOST)
    .then((res) => res.json())
    .then((data) => {
      res.send(`Hello! Current date: ${data}\n`)
    })
})

app.listen(3000, () => {
  console.log('Ready on port 3000!')
})
```

### Для запуска:
- node index.js

### Для получение ответа с сервера:
- curl localhost:3000

#### Для отключения:
- Ctrl + C

### Проверяем доступен ли нам Docker:
- docker -v

### Создаём файл Dockerfile:
```
FROM node:18

COPY . . 

RUN npm ci

EXPOSE 3000

CMD ["node", "index.js"]
```

### Создаём файл .dockerignore
```
node_modules
```

### Собираем образ Docker и сразу даем ему название "ci-cd-react":
- docker build -t ci-cd-react .

### Запускаем собранный образ с названием "ci-cd-react":
- docker run -d ci-cd-react


### Рекомендуемые команды для Docker:
#### посмотреть логи по id: 
- docker logs 5e83e90e9b099f11edb840355b9bed855f9514ba260eef512e294d003fc9fe35
#### посмотреть запущенные контейнеры: 
- docker ps
#### остановить запущенный контейнер по его имени: 
- docker stop ecstatic_elion
#### посмотреть все контейнеры вместе с остановленными: 
- docker ps -a
#### команда exec позволяет зайти внутри контейнера через Ubuntu по его имени в данном случае "naughty_bardeen" и выполнять в нем какие-либо команды например shell: 
- sudo docker exec -it naughty_bardeen /bin/sh
#### далее войдя можно например посмотреть содержимое контейнера:
- ls
#### чтобы выйти с контейнера на клавиатуре нажимаем сочетание клавиш:
- Ctrl + D
#### Чтобы удалить все образы:
- sudo docker system prune
#### Опубликовать порт в контейнере с названием "ci-cd-react" что бы общаться с ним:
- sudo docker run -p 1000:3000 -d ci-cd-react
#### проверить что связь с контейнером установлена, обращаемся к нему:
- curl localhost:1000
#### Добавляем в Dockerfile строку EXPOSE 3000 и пересобираем образ с новым названием "ci-cd-react":
docker build -t ci-cd-react .
#### проверяем что действительно пробросились порты: 
- docker ps
#### увидим: 0.0.0.0:1000->3000/tcp
#### останавливаем контейнер по id что бы не было конфликтов:
- docker stop d6678d1a567b
#### запускаем его по названию:
- docker run -d ci-cd-react
#### проверяем что привязки портов нет: 
- docker ps
#### вместо 0.0.0.0:1000->3000/tcp увидим: 3000/tcp 


### Идем на Docker Hub и создаем репозиторий с одноименным названием как в image: файла docker-compose.yml https://hub.docker.com/ => Create repository => Name: ci-cd-react-client => Create
### Собираем наши образы:
- docker compose build





### поднимаем docker
- docker compose up -d
### стучимся по порту 3000 поднятом Docker
- curl localhost:3000
### что бы положить все наши контейнеры:
- docker compose down
### что бы положить какой то конкретный контейнер например с id: a665e95e9d21:
- docker stop a665e95e9d21

## Переменные окружения
### Пересобираем контейнеры:
- docker compose build
### поднимаем docker
- docker compose up -d
### стучимся по порту 3000 поднятом Docker
- curl localhost:3000

## Что бы запушить образ в https://hub.docker.com:
- в https://hub.docker.com создаем репозиторий с названием ci-cd-react-client
- в файле docker-compose.yml добавляем поле image: vitalyvitmens/ci-cd-react-client
- docker compose build
- docker login
- docker compose push | docker push vitalyvitmens/ci-cd-react-client:latest

